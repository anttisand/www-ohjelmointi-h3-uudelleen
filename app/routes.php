<?php

return [
	'/' => 'HomeController',
	'tasks' => 'TasksController'
];

// Voidaan kirjoittaa koko polku, tai sopia, että kontrollerit sijoitetaan aina
// tiettyyn hakemistoon ja näin välttyä kirjoittamasta joka kerta koko polkua
// return [
// 	'/' => 'app/controllers/HomeController',
// 	'tasks' => 'app/controllers/TasksController'
// ];