<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>H3</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.3.0/css/bulma.min.css"/>
</head>
<body>

<section class="hero is-medium is-primary is-bold">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">WWW-ohjelmointi</title>
      <h2 class="subtitle">Todo -app</h2>
    </div>
  </div>
</section>
<div class="tabs is-centered">
  <ul>
    <li><a href="/">Etusivu</a></li>
    <li><a href="/tasks">Taskit</a></li>
  </ul>
</div>

    <section class="section">
        <div class="container">
