<?php

require 'app/models/Task.php';

$message = '';

// Käyttäjä lähetti lomakkeen tiedot
if(isset($_POST['description']))
{
	// Tarkistetaan, että molemmissa kentissä on merkkejä (huono validointi)
	if(strlen($_POST['description']) < 0 || strlen($_POST['duedate']) < 0) {
		$message = 'Anna tehtävälle kuvaus ja määräaika.';

	// Tarkistetaan, että päivämäärä on 1.1.1111 tai 01.01.1111 tai 01.1.1111 tai 1.01.1111
	} else if(!preg_match('/^\d{1,2}\.\d{1,2}\.\d{4}$/', $_POST['duedate'])) {
		$message = 'Anna määräaika muodossa dd.mm.yyyy';

	// Tarkistelut tehty, lisätään rivi kantaan
	} else {
		Task::create([
			'description' => $_POST['description'],
			'duedate' => $_POST['duedate']
		]);
	}
}

if(isset($_GET['action']))
{
	switch ($_GET['action']) {
		case 'merkkaa':
			if(isset($_GET['id']) && is_numeric($_GET['id'])) {
				Task::update($_GET['id'], [
					'completed' => true
				]);
				$message = "Tehtävä merkattu valmiiksi.";
			} else {
				$message = "Pyynnöstä puuttui kelvollinen id";
			}
			break;

		case 'poista':
			if(isset($_GET['id']) && is_numeric($_GET['id'])) {

				Task::delete($_GET['id']);

				$message = "Tehtävä poistettu.";

			} else {
				$message = "Pyynnöstä puuttui kelvollinen id";
			}
			break;

		default:
			$message = "Tuntematon toiminto. Käytä ['merkkaa', 'poista'].";
			break;
	}
}

$tasks = Task::all();

require 'app/resources/views/task.view.php';